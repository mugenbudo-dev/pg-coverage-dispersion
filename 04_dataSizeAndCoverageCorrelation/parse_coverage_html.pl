#!/bin/perl

use POSIX;
use strict;


open(OUTPUT,">>".$ENV{OUTFNAME}) or die("cant open file ".$ENV{OUTFNAME});

	
if (open(INDEX,"<" .$ENV{COVFNAME}."/src/backend/access/".$ENV{TARGET_MODULE}."/index.html"))
{
	my $indexName = "";
		
	while (my $line = <INDEX>)
	{
		my ($coverage) = ($line=~m[headerCovTableEntry\D+(\d+)]);
		
		if ($coverage)		# if this is one of the indexes we are looking for
		{
				print OUTPUT $coverage."\t\t";
				close(INDEX);
				close(OUTPUT);

				exit 0;
		}
	}
		
}
else
{
	die "cant open coverage file ".$ENV{COVFNAME}."/src/backend/access/".$ENV{TARGET_MODULE}."/index.html"
}
close(OUTPUT);
