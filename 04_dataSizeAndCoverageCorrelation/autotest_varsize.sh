#!/bin/bash

# LINEAR_GROWTH - if set to 1, datasize will increase by addiction of GROWTH_FACTOR and by multipilication with the latter otherwise
#
# GROWTH_FACTOR - multipiler (or delta if LINEAR_GROWTH=1) for datasize. E.g. GROWTH_FACTOR=2 results in x2, x4, x8 and so on
# multipilers for datasize
# 
# MAX_MULTIPILER - datasize multipiler wont go above said value (256 by default)
#
# INITIAL_MULTIPILER - starting point in datasize multiplication. Useful for when we already got data on lesser sizes and would like
# to skip unnesessary calculations.
#
# OUTFNAME - output file name.
#
# TESTS_LIST and TARGETS_LISTS - lists of tests and their target modules separated by whitespace. Order matters (eg "foo_test bar_test baz_test" and "foo bar baz")
#
# this script automatically replaces default tests with ones using env var to adjust datasize.
# It does NOT replace .out files in src/test/regress/expected folder, hence every test will fail. This, however, doesnt affect 
# their coverage. Tests will be replaced with originals after execution, but in case of mid-process kill saved tests cant
# be found in tmp folder.


thisDir=$(pwd)

if [[ -z "${GROWTH_FACTOR}" ]]
then
    export GROWTH_FACTOR=2
fi

if [[ -z "${MAX_MULTIPILER}" ]]
then
    export MAX_MULTIPILER=256
fi

if [[ -z "${INITIAL_MULTIPILER}" ]]
then
    export INITIAL_MULTIPILER=1
fi

if [[ -z "${OUTFNAME}" ]]
then
	export OUTFNAME="$thisDir/datasize_and_coverage_correlation.cvs"
fi

if ! test -f "$OUTFNAME"
then
    # if no file with such name exists, prepare (write out datasize multipilers) a new one
    touch "$OUTFNAME"
    echo -e -n "datasize multip.\t\t" >> "$OUTFNAME"

    for ((mul=$INITIAL_MULTIPILER;mul<=MAX_MULTIPILER;))
    do
        echo -e -n "$mul\t\t" >> "$OUTFNAME"
        if [[ $LINEAR_GROWTH == "1" ]]
            then
                mul=$((GROWTH_FACTOR+mul))
            else
                mul=$((GROWTH_FACTOR*mul))
            fi
    done
    echo "" >> "$OUTFNAME"
else
    read -r -p "File $OUTFNAME already exists. Do you want to continiue writing data to it? [Y/n]: " response
	if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
	then
    		echo "Stopping. Make sure output file name specified in OUTFNAME variable is not that of existing file (datasize_and_coverage_correlation.cvs by default)\n"
			exit 0
	fi
fi

if [[ -z "$TESTS_LIST" ]]
then
    declare -a tests=("brin" "gin" "gist" "btree_index" "hash_index" "spgist")
else
    declare -a tests=($TESTS_LIST)
fi

if [[ -z "$TARGETS_LIST" ]]
then
    declare -a targets=("brin" "gin" "gist" "nbtree" "hash" "spgist")
else
    declare -a targets=($TARGETS_LIST)
fi




if [[ -z "${PGDIST}" ]]
then
	# if existing directory isnt specified, clone, configure and make a new one
	export PGDIST="$(pwd)/../postgresql"
	echo -e "no postgres directory specified. If one exists, use PGDIST variable to specify it. Cloning..."
	git clone https://git.postgresql.org/git/postgresql.git $PGDIST
	
	cd $PGDIST
	make -s distclean
	./configure --enable-tap-tests --enable-coverage 1> /dev/null
	make -s
else
	cd $PGDIST		||	exit -1
	make -s coverage-clean
fi
cd $thisDir
export COVFNAME=$PGDIST/coverage                        # this script doesnt need to save any data, so we can just pass coverage folder
                                                        # that is in postgres directory to perl script. And since its location is constant we can
                                                        # avoid constantly reassigning this var


for ((testIndex=0;testIndex<${#tests[@]};testIndex++))  # might be not the best idea to rely on arrays being the same lenght
do                                                      # TODO add some checks (or a more robust way to set tests and targets)
    export TARGET_MODULE=${targets[$testIndex]}
    echo -e -n "\n"$TARGET_MODULE"\t\t" >> $OUTFNAME

    # save original test and replace it with a modified one
    cp $PGDIST/src/test/regress/sql/${tests[$testIndex]}".sql" $thisDir/tmp/${tests[$testIndex]}".sql"
    cp $thisDir/tests/${tests[$testIndex]}"_varsize.sql" $PGDIST/src/test/regress/sql/${tests[$testIndex]}".sql"

    # going through a loop of increasing datasize
    for ((mul=$INITIAL_MULTIPILER;mul<=MAX_MULTIPILER;))
    do
        export DATASIZEMUL=$mul
        
        # run test, get coverage, parse coverage and save it, clean coverage
        TESTS="test_setup create_index ${tests[$testIndex]}" make -C $PGDIST check-tests
        make -C $PGDIST coverage-html
        perl parse_coverage_html.pl
        make coverage-clean

        # increase multipiler 
        if [[ $LINEAR_GROWTH == "1" ]]
        then
            mul=$((GROWTH_FACTOR+mul))
        else
            mul=$((GROWTH_FACTOR*mul))
        fi
    done 

    # returning original test on its place
    cp $thisDir/tmp/${tests[$testIndex]}".sql" $PGDIST/src/test/regress/sql/${tests[$testIndex]}".sql"
    rm $thisDir/tmp/${tests[$testIndex]}".sql"
done
