#!/bin/bash

# this script runs multipile test parallel pairs in order to find the ones raising coverage of modules we are intrested in.
# By replacing original src/test/regress/parallel_schedule file with
# one of pre-made ones (see test parallels folder) containing one of test parallels (and its dependencies),
# running it a couple of times alongside with original test and comparing resulting coverage with the one we got in 2nd step
# we can find group(s) of tests responisible for coverage increase.
#
# if script execution was killed mid-process, ALWAYS run scripts/cleanup.sh from the same folder this script is in
# and remember to specify psql repo directory in PGDIST enviroment var

if [[ -z "${PARALLEL_TESTS_EXEC_COUNT}" ]]
then
	export PARALLEL_TESTS_EXEC_COUNT=10		# number of times group of parallel tests will be executed (greater -> more stable results but slower execution)
fi


PARALLELS_COUNT=20		# non-adjustable, highest index of test parallels

if [[ -z "${PGDIST}" ]]		# if directory isnt specified
then					# clone, configure, and use the cloned one
	thisDir=$(pwd)
	export PGDIST="$thisDir/../postgresql"
	echo "no postgres directory specified. Use PGDIST env variable to specify it. cloning..."
	git clone https://git.postgresql.org/git/postgresql.git $PGDIST

	cd $PGDIST
	make coverage-clean 1> /dev/null
	./configure --enable-tap-tests --enable-coverage 1> /dev/null
	make 1> /dev/null
	cd $thisDir
fi

make -C $PGDIST coverage-clean 1> /dev/null

if [[ -z "$TESTS_LIST" ]]
then
    declare -a tests=("brin" "gin" "gist" "btree_index" "hash_index" "spgist")
else
    declare -a tests=($TESTS_LIST)
fi

if [[ -z "$TARGETS_LIST" ]]
then
    declare -a targets=("brin" "gin" "gist" "nbtree" "hash" "spgist")
else
    declare -a targets=($TARGETS_LIST)
fi



for ((testIndex=0;testIndex<${#tests[@]};testIndex++))
do
	export TABLENAME="side_tests_coverage_${targets[$testIndex]}.cvs"


	export TARGET=${targets[$testIndex]}
	echo -e -n "$TARGET\n" >> $TABLENAME
	echo -e -n "parallel N.\t\tcoverage\n" >> $TABLENAME

	for ((i=1;i<=$PARALLELS_COUNT;i++))
	do
		echo -e -n "$i\t\t" >> $TABLENAME
		TESTS="test_setup create_index ${tests[$testIndex]}" make -C $PGDIST check-tests

		cp  "../testParallels/parallel_schedule$i" "$PGDIST/src/test/regress/parallel_schedule"

		for ((j=0;j<=$PARALLEL_TESTS_EXEC_COUNT;j++))
		do
			make -C $PGDIST check
		done

		make -C $PGDIST coverage-html	||	{
			echo -e "failed to run make coverage-html. Make sure you have GCOV and LCOV installed and enabled coverage."
			exit -1
		}						# making coverage report
		perl "scripts/parse_coverage_report.pl"	# and parsing it
		make -C $PGDIST coverage-clean
		echo "Finished $i/$PARALLELS_COUNT parallels"
	done
done


bash scripts/cleanup.sh		#replace parallel_schedule with original one
