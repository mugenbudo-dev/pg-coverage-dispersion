#!/bin/bash

# used to clean up mess caused by replacing original parallel_schedule file

cp  "../testParallels/parallel_schedule0" "$PGDIST/src/test/regress/parallel_schedule"
make -C $PGDIST coverage-clean
