#!/bin/perl

# parses html coverage report and writes line coverage at the end of specified output file
# executed after each parallel pair is runned, so 1st line in output is for pair 1-9 (or 1-10), 
# 2nd is for 2-9 etc


open(my $TABLE,">>".$ENV{TABLENAME});

open(my $COVERAGE,"<".$ENV{PGDIST}."/coverage/src/backend/access/".$ENV{TARGET}."/index.html") or die("no such dir, env was $ENV{PGDIST}");
	
while ($line = <$COVERAGE>)
{
	if (($lineCov) = ($line =~ m[CovTableEntry\D+(\d+)]))	# this regular expression is a pattern for lines containing coverage
	{
		print {$TABLE} $lineCov."\t";		# we need only 1st of the lines matching this pattern as it contains line coverage
							# and other containing total lines and fun coverage
		last;
	}
}


print {$TABLE} "\n";
