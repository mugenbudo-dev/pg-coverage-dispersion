#!/bin/perl

# creates output file and fills 1st line with index names, executed once before each test pair set is runned

@filenames = ("brin", "gin", "gist", "spgist", "nbtree", "hash");

open(my $TABLE,">".$ENV{TABLENAME}) or exit -1;

foreach $filename (@filenames)
{
	print {$TABLE} $filename."\t";
}

print {$TABLE} "\n"
