# Введение 

Покрытие, генерируемое штатными тестами Postgresql на данный момент не является постоянным.
Так, если запустить полный цикл тестов с одинаковой конфигурацией два раза подряд, можно заметить, что покрытие в результате выполнения первого 
отличается от оного в результате второго. 
Эта особенность приводит к невозможности трезво оценить эффективность (то бишь покрытие) тестов до и после их изменения (или, что чаще, изменения кода)
иначе как запуская несколько циклов тестов подряд. 
Целью данной работы ставится стабилизация покрытия штатных тестов 6 индексов - ```brin, gin, gist, spgist, hash и nbtree``` - путем увеличения размера таблиц(ы), используемых(ой) в тестовых запросах.

# Состояние на данный момент

Несмотря на то, что характер (и границы) разброса меняются по мере внесения изменений в master ветку БД, сам факт разброса остается неизменным. По результатам 100 запусков были получены следующие результаты:

![brin_01](01_fullCoverageData/charts/brin_coverage_step01.png)

![gin_01](01_fullCoverageData/charts/gin_coverage_step01.png)

![gist_01](01_fullCoverageData/charts/gist_coverage_step01.png)

![spgist_01](01_fullCoverageData/charts/spgist_coverage_step01.png)

![nbtree_01](01_fullCoverageData/charts/nbtree_coverage_step01.png)

![hash_01](01_fullCoverageData/charts/hash_coverage_step01.png)

Для удобства оценки характера разброса имеются графики с отсортированными по возрастанию столбцами:

![gin_sorted_01](01_fullCoverageData/charts/gin_sorted_coverage_step01.png)

![spgist_sorted_01](01_fullCoverageData/charts/spgist_coverage_sorted_step01.png)

![nbtree_sorted_01](01_fullCoverageData/charts/nbtree_sorted_coverage_step01.png)

Как можно видеть, покрытие у brin достаточно стабильное, в отличие отстальных индексов. Особо выделяется btree, распределение покрытия у которого относительно равномерное.

# Разброс штатных тестов

Факт наличия разброса в тестах с постоянными данными и операциями является странным, и потому было решено проверить есть ли дисперсия покрытия при запуске тестов исследуемых индексов отдельно от остальных.

На данном этапе несколько раз последовательно запускались тесты-зависимости (test_setup и create_index) и исследуемый тест. Данные о покрытии сохранялись для дальнейшей оценки. 
Результат - покрытие стабильное для всех индексов. В то же время выяснилось, что покрытие штатных тестов значительно ниже чем покрытие полного цикла (например, покрытие тестом brin и зависимостями - приблизительно 1300 строк кода, в то время как при запуске полного make check оно свыше 2000 строк).

![brin_02](02_standardTestDispersion/charts/brin_coverage_step02.png)

![gin_02](02_standardTestDispersion/charts/gin_coverage_step02.png)

![gist_02](02_standardTestDispersion/charts/gist_coverage_step02.png)

![spgist_02](02_standardTestDispersion/charts/spgist_coverage_step02.png)

![nbtree_02](02_standardTestDispersion/charts/nbtree_coverage_step02.png)

![hash_02](02_standardTestDispersion/charts/hash_coverage_step02.png)

# Влияние прочих тестов 

Так как покрытие штатного теста и полного цикла тестов отличаются, необходимо приблизительно понять, какие группы тестов дают дополнительное покрытие, так как причина разброса может быть в сторонних тестах.

План тестов был разделен на несколько групп параллельных тестов (и их зависимости), после чего каждая из этих групп запускалась вместе со штатным тестом. После чего сравнивалось покрытие штатного теста и запуска штатный+группа тестов. Как можно видеть на графиках ниже, сторонних тестов, влияющих на покрытие исследуемых индексов, достаточно много.

![brin_03](03_parallelPairsCoverage/charts/brin_coverage_influence_step03.png)

![gin_03](03_parallelPairsCoverage/charts/gin_coverage_influence_step03.png)

![gist_03](03_parallelPairsCoverage/charts/gist_coverage_influence_step03.png)

![spgist_03](03_parallelPairsCoverage/charts/spgist_coverage_influence_step03.png)

![nbtree_03](03_parallelPairsCoverage/charts/nbtree_coverage_influence_step03.png)

![hash_03](03_parallelPairsCoverage/charts/hash_coverage_influence_step03.png)

# Увеличение размера выборки и покрытие

Исходя из суждения, что покрытие стандартными тестами стабильно и источник разброса - другие тесты, было решено попробовать увеличить выборки в тестах индексов, дабы они стабильно покрывали "проблемные" куски кода.
На данном этапе размер выборки увеличивался экспоненциально, и после каждого запуска собирались данные о новом покрытии. В результате покрытие действительно росло вместе с размером таблиц:

![brin_04](04_dataSizeAndCoverageCorrelation/charts/brin_corretation_data_step04.png)

![gin_04](04_dataSizeAndCoverageCorrelation/charts/gin_corretation_data_step04.png)

![gist_04](04_dataSizeAndCoverageCorrelation/charts/gist_corretation_data_step04.png)

![spgist_04](04_dataSizeAndCoverageCorrelation/charts/spgist_corretation_data_step04.png)

![nbtree_04](04_dataSizeAndCoverageCorrelation/charts/nbtree_corretation_data_step04.png)

![hash_04](04_dataSizeAndCoverageCorrelation/charts/hash_corretation_data_step04.png)

В связи с огромными временными затратами (вплоть до суток) на выполнение тестов, предел роста покрытия был достигнут не везде. 

# Подбор оптимального размера выборки

С ростом покрытия выросло и время выполнения тестов, что пришлось учитывать при выборе нового размера выборки. На графиках ниже можно увидеть зависимость времени выполнения тестов в зависимости от множителя размера данных.

На данном этапе измерялось время выполнения тестов в зависимости от размера выборки. Вместе с результатами предыдущего этапа, эти данные помогут в подборе оптимального размера выборки.

![brin_05](05_optimalDatasize/charts/brin_execution_time_step05.png)

![gin_05](05_optimalDatasize/charts/gin_execution_time_step05.png)

![gist_05](05_optimalDatasize/charts/gist_execution_time_step05.png)

![btree_index_05](05_optimalDatasize/charts/btree_index_execution_time_step05.png)

![spgist_05](05_optimalDatasize/charts/spgist_execution_time_step05.png)

![hash_index_05](05_optimalDatasize/charts/hash_index_execution_time_step05.png)

Для последних двух так же приводятся логарифмические графики:

![spgist_log_05](05_optimalDatasize/charts/spgist_execution_time_logarithmic_step05.png)

![hash_index_log_05](05_optimalDatasize/charts/hash_index_execution_time_logarithmic_step05.png)

Оптимальным временем выполнения было положено не более 30 секунд (как выяснилось позже, при запуске полного make check время выполнения увеличивается). В результате были взяты следующие множители:

- brin - x64
- gin - x8
- gist - x2
- spgist - x16
- hash - x25
- btree - x2


# Результаты выполнения новых тестов

Несмотря на то, что при изучении зависимости покрытия от размера выборки на теста на тестах, запускаемых отдельно, рост покрытия был не очень большим, при запуске вместе с остальными разница между "до" и "после" стала куда больше. 

Так, покрытие индекса gin стабилизировалось и, более того, выросло. Ширина колебаний нового покрытия - ~6 строк Результаты можно видеть на графике ниже:

![gin_comp_06](06_adjustedTestsCoverage/charts/gin_before_and_after_comparsion_step06.png)

Тест gist на данный момент обеспечивает достаточно стабильное покрытие. С ростом выборки, однако, покрытие стало нестабильным, с разницей минимального и максимального покрытия в ~70 строк кода строк кода (против 2-3 строк разницы до). Однако покрытие нового теста значительно выше по сравнению со старым. 

![gist_comp_06](06_adjustedTestsCoverage/charts/gist_before_and_after_comparsion_step06.png)

При этом индекс spgist ведет себя странно: при увеличении выборки прочих тестов его покрытие стабилизировалось в сторону роста, однако после подстановки нового теста spgist с увеличенной выборкой покрытие просело. Поэтому тест spgist не корректировался. Новое покрытие можно увидеть на графике ниже.

![spgist_comp_06](06_adjustedTestsCoverage/charts/spgist_before_and_after_comparsion_step06.png)

Пусть среднее покрытие hash и выросло, разброс покрытия стал заметнее, что противоречит изначальной цели:

![hash_comp_06](06_adjustedTestsCoverage/charts/hash_before_and_after_comparsion_step06.png)

Поэтому изменения в hash_index было решено отбросить.

Индекс btree, являющийся самым "проблемным" из всех по причине равномерного распределения покрытия (против скачка у других), однако, удалось несколько стабилизировать. Хотя дальнейшее увеличение выборки и может дать еще большую утяжку разброса, встает вопрос оптимальности по времени выполнения теста. Сравнить старое и новое покрытие можно по графику ниже:

![nbtree_comp_06](06_adjustedTestsCoverage/charts/nbtree_before_and_after_comparsion_step06.png)

Подводя итоги, основной причиной разброса, вероятно, был не разброс покрытия сторонего(их) теста(ов), а их взаимодействие с основными. С ростом выборки покрытие, однако, по большей части стабилизировалось.