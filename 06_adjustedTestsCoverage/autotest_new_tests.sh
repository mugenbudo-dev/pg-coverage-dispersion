#!/bin/bash

# this script just runs full check multiple times at psql_path/src direcrory
# and saves coverage reports to data folder

if [[ -z "${NTESTS}" ]]
then
	NTESTS=100		# number of checks that will be runned, adjustable
fi

thisDir=$(pwd)
if [[ -z "${PGDIST}" ]]
then
	# if existing directory isnt specified, clone, configure and make a new one
	export PGDIST="$(pwd)/../postgresql"
	echo -e "no postgres directory specified. If one exists, use PGDIST variable to specify it. Cloning..."
	git clone https://git.postgresql.org/git/postgresql.git $PGDIST

	cd $PGDIST
	make -s distclean
	./configure --enable-tap-tests --enable-coverage 1> /dev/null
	make -s
else
	cd $PGDIST		||	exit -1
	make -s coverage-clean
fi

cd $thisDir

# replacing original test with our new ones
# this assumes there is a 5th step dir with new tests
#
# the trick here is to cd to folder with tests,make for loop go through a sequence of raw filenames and jump back to our dir
# this way loop var is just a filename, w/o path prefix, and we dont need to think whether PGDIST is relative path or absolute
cd ../05_optimalDatasize/new_tests_proposal/sql
for testFile in *.sql
do
	cd $thisDir
	cp $PGDIST/src/test/regress/sql/$testFile tmp/$testFile
	cp ../05_optimalDatasize/new_tests_proposal/sql/$testFile $PGDIST/src/test/regress/sql/$testFile
done
cd ../05_optimalDatasize/new_tests_proposal/expected
for expFile in *.out
do
	cd $thisDir
	cp $PGDIST/src/test/regress/expected/$expFile tmp/$expFile
	cp ../05_optimalDatasize/new_tests_proposal/expected/$expFile $PGDIST/src/test/regress/expected/$expFile
done

cd $thisDir

if [[ -z "${OUTFNAME}" ]]
then
	export OUTFNAME="$thisDir/coverage_data_new_tests.cvs"
fi

if test -f "$OUTFNAME"
then
	read -r -p "File $OUTFNAME already exists. Do you want to continiue writing data to it? [Y/n]: " response
	if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
	then
    		echo "Stopping. Make sure output file name specified in OUTFNAME variable is not that of existing file (coverage_data_new_tests.cvs by default)\n"
			exit 0
	fi
else
	perl $thisDir/prep_cvs.pl
fi


export COVFNAME="$PGDIST/coverage"

echo "Running tests..."

for ((i=1;i<=$NTESTS;i++))
do
	make -C $PGDIST -s check	||	{
		echo -e "failed to run make check. Make sure youve run ./configure with --enable-tap-tests for psql"
		exit -1
	}
	make -C $PGDIST -s coverage-html	||	{
		echo -e "failed to run make coverage-html. Make sure you have GCOV and LCOV installed and enabled coverage."
		exit -1
	}
	perl $thisDir/parse_coverage_html.pl
	make -C $PGDIST coverage-clean 1> /dev/null
	echo -e "Tests executed: $i/$NTESTS"
done

# returning old tests to their rightful place
cd tmp
for saved in *.sql
do
	cd $thisDir
	cp tmp/$saved $PGDIST/src/test/regress/sql/$saved
done
cd tmp
for saved in *.out
do
	cd $thisDir
	cp tmp/$saved $PGDIST/src/test/regress/expected/$saved
done

cd $thisDir
