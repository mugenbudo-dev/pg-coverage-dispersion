#!/bin/perl

# this script parses previously generated html coverage reports and writes results to .cvs file


use POSIX;
use strict;

my $pattern = "/index.html\">src/backend/access/";

my @indexes = ("brin", "gin", "gist", "hash", "nbtree", "spgist");

open(OUTPUT,">>".$ENV{OUTFNAME});

my $lineCounter = -1;    # 5th line after marker is coverage
				  		 # -1 - marker not met, no incrementing
my $indexCounter = 0;	  
	
if (open(INDEX,"<".$ENV{COVFNAME}."/index.html"))
{
	my $indexName = "";
		
	while (my $line = <INDEX>)
	{
		if ($lineCounter == 5)							# stop on 5th row to gather line coverage data
			{
			my ($coverage) = ($line=~m[coverNum\D+(\d+) /]);
			if (grep( /^$indexName$/, @indexes))		# if this is one of the indexes we are looking for
			{
				print OUTPUT "$coverage\t\t";
			}
			$indexName="";								# nullify counter and index name. Loop will search for next indexName entry
			$lineCounter = -1;
			
		}
		elsif ($indexName eq "")	
		{								
			if (($indexName) = ($line =~ m[$pattern(\S+)</a></td>]))	# if found a new index,
			{
				$lineCounter = 0;					# start counting 5 lines till line with coverage data
				$indexCounter++;
			
			}
		}
		
		if ($lineCounter != -1)						# only increment if we met a marker line
		{
			$lineCounter++;
		}
	}
    	print OUTPUT "\n";
		
	close(INDEX);
}
else
{
	die "cant open coverage folder ".$ENV{COVFNAME};
}

close(OUTPUT);

