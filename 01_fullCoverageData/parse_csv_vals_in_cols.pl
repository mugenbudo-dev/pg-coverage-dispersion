#!/bin/perl

use strict;


open (OUTPUT, ">coverage_summary_vals_in_cols_new.csv");
print OUTPUT "filename\t\ttotal_lines";
for my $j (1..100)
{
	print OUTPUT "\t\t".$j;
}
print OUTPUT "\n";

for (my $lineItr=0; $lineItr<=1439; $lineItr++)		# 1439 is for number of lines in .csv's
								# on each iteration, we open all of the .csv's and read i'th line
{
    my $caughtName=0;						# if 0, write file/folder name and total lines in OUTPUT. Will be set for 1 after 1st file had been parsed
    								# using this var is basically the same thing as checking if we are parsing 1st 
    								# .csv file
    for my $i (1..100)
    {
        open (TABLE, "<tmp/coverage".$i.".csv") or open (TABLE, "<tmp/coverage".$i.".cvs");   
        my @lines = <TABLE>;
        my $line = $lines[$lineItr];
        

        if ($line)
        {
            if ($caughtName==0)
            {
                (my $name) = $line=~m[([/.a-zA-Z ]+)\t\t\d+];    	# 1st col is for directory/filename
                (my $total) = $line=~m[\t(\d+)];        		# 3rd col (or 2nd number in this case) is for total lines
                $caughtName=1;
                print OUTPUT $name."\t\t".$total;
            }

            (my $covered) = $line=~m[\t(\d+)\t];        # 2nd col is for lines covered
            print OUTPUT "\t\t".$covered;
        }
    }

    print OUTPUT "\n";
}
