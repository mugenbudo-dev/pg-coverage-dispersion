#!/bin/bash

# this script just runs full check multiple times at psql_path/src direcrory
# and saves coverage reports to data folder

PGDIST=""
NTESTS=100
while [ -n "$1" ]
do
	case "$1" in
		-pgdist) PGDIST="$2" ; shift ;;
		-ntests) NTESTS="$2" ; shift ;;
		*) echo "unknown param: $1"  ;;
	esac
	shift
done


thisDir=$(pwd)
if [[ -z "$PGDIST" ]]
then
	# if existing directory isnt specified, clone, configure and make a new one
	PGDIST="$(pwd)/../postgresql"
	echo -e "no postgres directory specified. If one exists, use -pgdist [directory] to specify it. Cloning..."
	git clone https://git.postgresql.org/git/postgresql.git $PGDIST

	cd $PGDIST
	make -s distclean
	./configure --enable-tap-tests --enable-coverage 1> /dev/null
	make -s
else
	cd $PGDIST		||	exit -1
	make -s coverage-clean
fi

cd $thisDir

echo "Running tests..."

for ((i=1;i<=$NTESTS;i++))
do
	make -C $PGDIST -s check	||	{
		echo -e "failed to run make check. Make sure youve run ./configure with --enable-tap-tests for psql"
		exit -1
	}
	make -C $PGDIST -s coverage-html	||	{
		echo -e "failed to run make coverage-html. Make sure you have GCOV and LCOV installed and enabled coverage."
		exit -1
	}
	perl parse_html.pl "$PGDIST/coverage" "coverage" $i
	make -C $PGDIST coverage-clean 1> /dev/null
	echo -e "Tests executed: $i/$NTESTS"
done

cd $thisDir
perl parse_csv_vals_in_cols.pl
perl parse_csv_vals_in_rows.pl
