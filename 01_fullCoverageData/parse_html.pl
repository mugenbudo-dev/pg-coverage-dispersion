#!/bin/perl

# this script parses previously generated html coverage reports and writes results to .csv file


use POSIX;
use strict;

(my $covFName, my $outFName, my $testIndex) = @ARGV;

open(OUTPUT,">tmp/".$outFName.$testIndex.".csv");

if (open(INDEX,"<".$covFName."/index.html"))
{
    my $globalCovsCaught = 0;

		
	while (my $line = <INDEX>)
	{

        if ((my $globalCoverage) = ($line=~m[<td class="headerCovTableEntry">(\d+)]))	# here we catch total coverage
        {
            if ($globalCovsCaught==0)								# first regex match is a total lines covered number
            {
                print OUTPUT "total_coverage\t\t".$globalCoverage."\t\t";
            }
            elsif ($globalCovsCaught==1)							# and the 2nd one is total lines of code
            {
                print OUTPUT $globalCoverage."\n\n";
            }											# 3rd ab=nd 4th are for functions, so we dont write them down
            $globalCovsCaught++;
        }

        if (my ($moduleDir) = ($line =~ m[<td class="coverFile"><a href="(\S+)/index.html"]))		# and here we get folder name
        {
            open(MODULE_INDEX, "<".$covFName."/".$moduleDir."/index.html");				# open its own index.html file
            my $coveragesCaught = 0;
            my $caughtSubmodule = 0;
            while (my $moduleLine = <MODULE_INDEX>)								# and write out its path and coverage
            {
                if ((my $moduleCoverage) = ($moduleLine =~ m[<td class="headerCovTableEntry">(\d+)]))
                {
                    # same logic as for getting global coverage
                    if ($coveragesCaught==0)
                    {
                        print OUTPUT $moduleDir."\t\t".$moduleCoverage."\t\t";
                    }
                    elsif ($coveragesCaught==1)
                    {
                        print OUTPUT $moduleCoverage."\n";
                    }

                    $coveragesCaught++;
                }

                if ((my $submoduleName) = ($moduleLine=~m[File"><a href="(\S+).gcov]))	# upon finding filename, write it out and raise a flag 
                {
                    print OUTPUT $submoduleName."\t\t";
                    $caughtSubmodule = 1;
                }
                
                # file's coverage will be found a few lines later, and it will be the first number matching 
                # following regex (with the next ones being total lines and function coverage.
                # upon getting coverage, lower the flag
                if (($caughtSubmodule) and ((my $submoduleCoverage, my $submoduleMax) = ($moduleLine=~m[<td class="coverNum\S+>(\d+) / (\d+)])))
                {
                    $caughtSubmodule = 0;
                    print OUTPUT $submoduleCoverage."\t\t".$submoduleMax."\n";
                }

            }

            print OUTPUT "\n";
        }
	}
	close(INDEX);
}
else
{
	die "cant open coverage folder ".$covFName;
}

close(OUTPUT);

