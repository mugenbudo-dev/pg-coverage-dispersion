#!/bin/perl

use strict;


open (OUTPUT, ">coverage_summary_vals_in_rows_new.csv");	

open (TABLE, "<tmp/coverage1.csv") or open (TABLE, "<tmp/coverage1.cvs"); 			# prepare output file by writing out all of the file/folder names (just copy them from 1st file)
print OUTPUT "filename\t\t";  
while (my $line = <TABLE>)
{
    (my $name) = $line=~m[([/.a-zA-Z ]+)];
    print OUTPUT $name."\t";
}

close TABLE;
print OUTPUT "\n";
open (TABLE, "<tmp/coverage1.csv") or open(TABLE, "<tmp/coverage1.cvs");   

print OUTPUT "total_lines\t\t"; 			# add total lines value in 2nd line
while (my $line = <TABLE>)
{
    (my $total) = $line=~m[\d+\t\t(\d+)];
    print OUTPUT $total."\t";
}
close TABLE;
print OUTPUT "\n";

for my $i (1..100)
{
            print OUTPUT $i."\t\t";
            open (TABLE, "<tmp/coverage".$i.".csv") or open (TABLE, "<tmp/coverage".$i.".cvs");   
            while (my $line = <TABLE>)
            {
                (my $cov) = $line=~m[(\d+)\t];	# this regex returns 1st match, and sub-tables are in name-covered-total format
                print OUTPUT $cov."\t";
            }
            close TABLE;
        

        print OUTPUT "\n";
    
}

close OUTPUT;
