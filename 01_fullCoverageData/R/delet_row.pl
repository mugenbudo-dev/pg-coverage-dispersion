#!/usr/bin/perl

use strict;

my $in_name = "../coverage_summary_vals_in_rows.cvs";
my $out_name = "truncated_coverage_summary_vals_in_rows.csv";
open my $in_file, '<', $in_name or die "Невозможно открыть файл $in_name: $!\n";
open my $out_file, '>', $out_name or die "Невозможно открыть файл $out_name: $!\n";

while(my $s = <$in_file>)
{
	$s =~ tr/\/ /_/ if $. == 1;
	next if $. == 2;
	print $out_file $s;
	#	last if $. == 4;
}

close $in_file;
close $out_file;
