#!/bin/perl

use POSIX;
use strict;

my $pattern = "<td class=\"headerCovTableEntry\">";

open(OUTPUT,">>".$ENV{OUTFNAME});

	
if (open(INDEX,"<".$ENV{COVFNAME}."/src/backend/access/".$ENV{TARGET_MODULE}."/index.html"))
{
	my $indexName = "";
		
    print "searching...\n";
	while (my $line = <INDEX>)
	{
        my ($coverage) = ($line=~m[$pattern(\d+)]);
		if ($coverage)		# if this is one of the indexes we are looking for
		{
				print OUTPUT "$coverage\t\t";
				close(INDEX);
				close(OUTPUT);
                print "gotcha!\n";
				exit 0;
		}
	}
		
}
else
{
	die "cant open coverage file ".$ENV{COVFNAME}."src/backend/access/".$ENV{TARGET_MODULE}."/index.html";
}
close(OUTPUT);
