#!/bin/bash

# OUTFNAME - output file name.
#
# TESTS_LIST and TARGETS_LISTS - lists of tests and their target modules separated by whitespace. Order matters (eg "foo_test bar_test baz_test" and "foo bar baz")
#
# This script just runs a bunch of make check-tests and gathers coverage data after each execution.
# While it doesnt replaces any files in postgres directory, you still should run [make coverage-clean] if it was killed mid-process.


thisDir=$(pwd)

if [[ -z "${NTESTS}" ]]
then
	export NTESTS=100		# number of checks that will be runned, adjustable
fi

if [[ -z "${OUTFNAME}" ]]
then
	export OUTFNAME="$thisDir/standalone_tests_coverage.cvs"
fi

if ! test -f "$OUTFNAME"
then
    # if no file with such name exists, prepare (write out datasize multipilers) a new one
    touch "$OUTFNAME"
    echo -e -n "Execution N.\t\t" >> "$OUTFNAME"

    for ((i=1;i<=NTESTS;i++))
    do
        echo -e -n "$i\t\t" >> "$OUTFNAME"
        if [[ $LINEAR_GROWTH == "1" ]]
            then
                mul=$((GROWTH_FACTOR+mul))
            else
                mul=$((GROWTH_FACTOR*mul))
            fi
    done
    echo "" >> "$OUTFNAME"
else
    read -r -p "File $OUTFNAME already exists. Do you want to continiue writing data to it? [Y/n]: " response
	if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
	then
    		echo "Stopping. Make sure output file name specified in OUTFNAME variable is not that of existing file (standalone_tests_coverage.cvs by default)\n"
			exit 0
	fi
fi

if [[ -z "$TESTS_LIST" ]]
then
    declare -a tests=("brin" "gin" "gist" "btree_index" "hash_index" "spgist")
else
    declare -a tests=($TESTS_LIST)
fi

if [[ -z "$TARGETS_LIST" ]]
then
    declare -a targets=("brin" "gin" "gist" "nbtree" "hash" "spgist")
else
    declare -a targets=($TARGETS_LIST)
fi



export COVFNAME=$PGDIST/coverage                        # this script doesnt need to save any data, so we can just pass coverage folder
                                                        # that is in postgres directory to perl script. And since its location is constant we can
                                                        # avoid constantly reassigning this var


if [[ -z "${PGDIST}" ]]
then
	# if existing directory isnt specified, clone, configure and make a new one
	export PGDIST="$(pwd)/../postgresql"
	echo -e "no postgres directory specified. If one exists, use PGDIST variable to specify it. Cloning..."
	git clone https://git.postgresql.org/git/postgresql.git $PGDIST

	cd $PGDIST
	make -s distclean
	./configure --enable-tap-tests --enable-coverage 1> /dev/null
	make -s
else
	cd $PGDIST		||	exit -1
	make -s coverage-clean
fi
cd $thisDir


for ((testIndex=0;testIndex<${#tests[@]};testIndex++))  # might be not the best idea to rely on arrays being the same lenght
do                                                      # TODO add some checks (or a more robust way to set tests and targets)
    export TARGET_MODULE=${targets[$testIndex]}
    echo -e -n "\n"$TARGET_MODULE"\t\t" >> $OUTFNAME


    for ((i=1;i<=NTESTS;i++))
    do

        # run test, get coverage, parse coverage and save it, clean coverage
        TESTS="test_setup create_index ${tests[$testIndex]}" make -C $PGDIST check-tests
        make -C $PGDIST coverage-html
        perl $thisDir/parse_coverage_html_02.pl
        make coverage-clean
    done
done
