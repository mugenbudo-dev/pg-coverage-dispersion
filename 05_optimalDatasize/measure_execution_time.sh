#!/bin/bash

# LINEAR_GROWTH - if set to 1, datasize will increase by addiction of GROWTH_FACTOR and by multipilication with the latter otherwise
#
# GROWTH_FACTOR - multipiler (or delta if LINEAR_GROWTH=1) for datasize. E.g. GROWTH_FACTOR=2 results in x2, x4, x8 and so on
# multipilers for datasize
# 
# MAX_MULTIPILER - datasize multipiler wont go above said value (256 by default)
#
# INITIAL_MULTIPILER - starting point in datasize multiplication. Useful for when we already got data on lesser sizes and would like
# to skip unnesessary calculations.
#
# OUTFNAME - output file name.
#
# TESTS_LIST and TARGETS_LISTS - lists of tests and their target modules separated by whitespace. Order matters (eg "foo_test bar_test baz_test" and "foo bar baz")
#
# this script automatically replaces default tests with ones using env var to adjust datasize and tracks their execution time.
# Execution time is measured in seconds, so 7 8 11 in output file stands for 7 seconds, 8 seconds and 11 seconds.
# Execution time CAN BE NEGATIVE. This is normal, since measurement error is around 1-2 secs. Since we dont need milsecond precision, its safe to replace 
# -1's and 0's with 1's.
# It does NOT replace .out files in src/test/regress/expected folder, hence every test will fail. 
# Since this script doesnt collect coverage data, it won't configure cloned postgres src to enable coverage
# This script is almost identical to the one in 4th step


thisDir=$(pwd)

if [[ -z "${GROWTH_FACTOR}" ]]
then
    export GROWTH_FACTOR=2
fi

if [[ -z "${MAX_MULTIPILER}" ]]
then
    export MAX_MULTIPILER=256
fi

if [[ -z "${INITIAL_MULTIPILER}" ]]
then
    export INITIAL_MULTIPILER=1
fi

if [[ -z "${OUTFNAME}" ]]
then
	export OUTFNAME="$thisDir/execution_time.cvs"
fi

if ! test -f "$OUTFNAME"
then
    # if no file with such name exists, prepare (write out datasize multipilers) a new one
    touch "$OUTFNAME"
    echo -e -n "datasize multip.\t\t" >> "$OUTFNAME"

    for ((mul=$INITIAL_MULTIPILER;mul<=MAX_MULTIPILER;))
    do
        echo -e -n "$mul\t\t" >> "$OUTFNAME"
        if [[ $LINEAR_GROWTH == "1" ]]
            then
                mul=$((GROWTH_FACTOR+mul))
            else
                mul=$((GROWTH_FACTOR*mul))
            fi
    done
    echo "" >> "$OUTFNAME"
else
    read -r -p "File $OUTFNAME already exists. Do you want to continiue writing data to it? [Y/n]: " response
	if [[ ! "$response" =~ ^([yY][eE][sS]|[yY])$ ]]
	then
    		echo "Stopping. Make sure output file name specified in OUTFNAME variable is not that of existing file\n"
			exit 0
	fi
fi

if [[ -z "$TESTS_LIST" ]]
then
    declare -a tests=("brin" "gin" "gist" "btree_index" "hash_index" "spgist")
else
    declare -a tests=($TESTS_LIST)
fi




if [[ -z "${PGDIST}" ]]
then
	# if existing directory isnt specified, clone, configure and make a new one
	export PGDIST="$(pwd)/../postgresql"
	echo -e "no postgres directory specified. If one exists, use PGDIST variable to specify it. Cloning..."
	git clone https://git.postgresql.org/git/postgresql.git $PGDIST
	
	cd $PGDIST
	make -s distclean
	./configure --enable-tap-tests 1> /dev/null
	make -s
else
	cd $PGDIST		||	{
        echo "Invalid directory: $PGDIST"
        exit -1
    }
	make -s coverage-clean
fi
export COVFNAME=$PGDIST/coverage                        # this script doesnt need to save any data, so we can just pass coverage folder
                                                        # that is in postgres directory to perl script. And since its location is constant we can
                                                        # avoid constantly reassigning this var
cd $thisDir

start=`date +%s`
     TESTS="test_setup create_index" make -C $PGDIST check-tests
end=`date +%s`
startupTime=$((end-start))

for ((testIndex=0;testIndex<${#tests[@]};testIndex++)) 
do                                                      
    export TARGET_MODULE=${test[$testIndex]}			# TODO rename, its not a module we are intrested at
    echo -e -n "\n"$TARGET_MODULE"\t\t" >> $OUTFNAME

    # save original test and replace it with a modified one
    cp $PGDIST/src/test/regress/sql/${tests[$testIndex]}".sql" $thisDir/tmp/${tests[$testIndex]}".sql"
    cp $thisDir/tests/${tests[$testIndex]}"_varsize.sql" $PGDIST/src/test/regress/sql/${tests[$testIndex]}".sql"

    # going through a loop of increasing datasize
    for ((mul=$INITIAL_MULTIPILER;mul<=MAX_MULTIPILER;))
    do
    	export DATASIZEMUL=$mul
        start=`date +%s`
            TESTS="test_setup create_index ${tests[$testIndex]}" make -C $PGDIST check-tests
        end=`date +%s`
        echo -e -n $((end-start-startupTime))"\t\t" >> $OUTFNAME

        # increase multipiler 
        if [[ $LINEAR_GROWTH == "1" ]]
        then
            mul=$((GROWTH_FACTOR+mul))
        else
            mul=$((GROWTH_FACTOR*mul))
        fi
    done 

    # returning original test on its place
    cp $thisDir/tmp/${tests[$testIndex]}".sql" $PGDIST/src/test/regress/sql/${tests[$testIndex]}".sql"
    rm $thisDir/tmp/${tests[$testIndex]}".sql"
done
